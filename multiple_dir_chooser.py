from PyQt5 import QtWidgets
import sys

class FileDialog(QtWidgets.QFileDialog):
    """
    Initializes a multi directory chooser
    """
    def __init__(self, inputdir, *args):
        QtWidgets.QFileDialog.__init__(self, *args)
        self.setOption(self.DontUseNativeDialog, True)
        self.setFileMode(self.Directory)
        self.setOption(self.ShowDirsOnly, True)
        self.setDirectory(inputdir)
        self.findChildren(QtWidgets.QListView)[0].setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.findChildren(QtWidgets.QTreeView)[0].setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

def getdata(dir):
    """
    Show the dialog and return the dirs
    :return: list of dirs
    """
    app = QtWidgets.QApplication(sys.argv)
    dlg = FileDialog(inputdir=dir)
    if dlg.exec_() == QtWidgets.QDialog.Accepted:
        dirlist = dlg.selectedFiles()

    return dirlist

# if __name__ == '__main__':
#     dirlist = getdata()
#     print(dirlist)

