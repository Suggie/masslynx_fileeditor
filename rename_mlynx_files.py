import os
import tkinter
import tkinter.simpledialog as simpledialog
import multiple_dir_chooser
from collections import Counter
import time


def read_externfile(file):
    with open(file, 'r') as extfile:
        ext_inf = extfile.read().splitlines()
        for line in ext_inf:
            if line.startswith('HeliumCellGasFlow'):
                he_gas_flow = line.split('/t')[0]
                he_gas_flow = float(he_gas_flow[21:])
                # print(he_gas_flow)
            if line.startswith('IMS Gas Flow'):
                im_gas_flow = line.split('/t')[0]
                im_gas_flow = float(im_gas_flow[21:])
                # print(im_gas_flow)
            if line.startswith('IMS Wave Velocity'):
                wave_vel = line.split('/t')[0]
                wave_vel = float(wave_vel[23:])
                # print(wave_vel)
            if line.startswith('IMS Wave Height'):
                wave_ht = line.split('/t')[0]
                wave_ht = float(wave_ht[19:])
                # print(wave_ht)
            if line.startswith('Start Mass'):
                start_mass = line.split('/t')[0]
                start_mass = float(start_mass[10:])
                # print(start_mass)
            if line.startswith('End Mass'):
                end_mass = line.split('/t')[0]
                end_mass = float(end_mass[8:])
                # print(end_mass)
        extfile.close()
    return he_gas_flow, im_gas_flow, wave_vel, wave_ht, start_mass, end_mass

if __name__ == '__main__':
    inputdir = r"C:\MassLynx\Default.pro\Data"
    dirs = multiple_dir_chooser.getdata(inputdir)
    # print(dirs)
    basedir = os.path.dirname(dirs[-1])
    root = tkinter.Tk()
    root.withdraw()
    var = simpledialog.askstring("File Name", "Enter the name to append at the begining")
    start_time = time.time()
    copy_num = 1
    old_paths_list, new_name_list = [], []
    for dir in dirs:
        listfiles = os.listdir(dir)
        dirpath = os.path.abspath(dir)
        old_paths_list.append(dirpath)
        # print(dirpath)
        file = [x for x in listfiles if x.endswith('extern.inf')]
        ext_file = os.path.join(dirpath, file[0])
        he_flow, im_flow, wave_vel, wave_ht, start_mass, end_mass = read_externfile(ext_file)
        new_name = ('_').join([var, str(int(he_flow)), str(int(im_flow)), str(int(start_mass)), str(int(end_mass)), str(int(wave_vel)), str(int(wave_ht))])+'_'+str(0)+str(0)+str(copy_num)+'.raw'
        new_name_list.append(new_name)

    counts = Counter(new_name_list)
    for name, num in counts.items():
        if num > 1:
            for cp_num in range(2, num+1):
                name_vars = name.split('_')
                name_nocpnum = ('_').join([x for x in name_vars[0:-1]])
                new_name_list[new_name_list.index(name)] = name_nocpnum+'_'+str(0)+str(0)+str(cp_num)+'.raw'

    for ind, name in enumerate(new_name_list):
        newpath = os.path.join(basedir, name)
        os.rename(old_paths_list[ind], newpath)

    print("--- Renaming files took %s seconds ---"% (time.time() - start_time))